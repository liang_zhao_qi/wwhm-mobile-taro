export default {
  pages: [
    'pages/tabBar/index/index',
    'pages/tabBar/personal/index'
  ],
  subpackages: [
    {
      root: "pagesA",
      pages: [
        "test/index"
      ]
    }
  ],
  tabBar: {
    color: "#7A7E83",
    selectedColor: "#1296db",
    borderStyle: "black",
    backgroundColor: "#ffffff",
    list: [
      {
        pagePath: "pages/tabBar/index/index",
        iconPath: "static/icon/Home.png",
        selectedIconPath: "static/icon/HomeTrue.png",
        text: "首页"
      },
      {
        pagePath: "pages/tabBar/personal/index",
        iconPath: "static/icon/Personal.png",
        selectedIconPath: "static/icon/PersonalTrue.png",
        text: "我的"
      }
    ]
  },
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
}
