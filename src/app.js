/*
 * @Date: 2020-08-27 19:25:26
 * @LastEditors: liang_zhaoqi
 * @Email: 2394690057@qq.com
 * @LastEditTime: 2020-08-27 19:30:20
 * @FilePath: \wwhm-mobile-Taro\src\app.js
 * @Synopsis: 
 */
import { Component } from 'react'
import './app.styl'
import 'taro-ui/dist/style/index.scss'

class App extends Component {

  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  componentDidCatchError () {}

  // this.props.children 是将要会渲染的页面
  render () {
    return this.props.children
  }
}

export default App
